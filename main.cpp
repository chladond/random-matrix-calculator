#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

//Class representing ComplexNumber
class ComplexNumber {
private:
    double re;
    double im;
public:

    //Constructor, creates object ComplexNumber
    //@param real = real part of ComplexNumber
    //@param imaginary = imaginary part of ComplexNumber
    ComplexNumber(double real, double imaginary) {
        re = real;
        im = imaginary;
    }

    //Overloaded operator for multiplication of ComplexNumber
    ComplexNumber *operator*(const ComplexNumber &complexNumber) const;

    //Overloaded operator for addition of ComplexNumber
    ComplexNumber *operator+(const ComplexNumber &complexNumber) const {
        return new ComplexNumber(this->getReal() + complexNumber.getReal(),
                                 this->getImaginary() + complexNumber.getImaginary());
    }

    //Overloaded operator for subtraction of ComplexNumber
    ComplexNumber * operator-(const ComplexNumber &complexNumber) const {
        return new ComplexNumber(this->getReal() - complexNumber.getReal(),
                                 this->getImaginary() - complexNumber.getImaginary());
    }

    //Returns real part of ComplexNumber
    double getReal() const {
        return this->re;
    }

    //Returns imaginary part of ComplexNumber
    double getImaginary() const {
        return this->im;
    }

    //Representing data into string
    string toString() {
        return to_string(this->re) + " " + " " + to_string(this->im) + "i";
    }

};

ComplexNumber *ComplexNumber::operator*(const ComplexNumber &complexNumber) const {
    return new ComplexNumber(
            this->getReal() * complexNumber.getReal() - this->getImaginary() * complexNumber.getImaginary(),
            this->getImaginary() * complexNumber.getReal() + this->getReal() * complexNumber.getImaginary());
}

//Class representing Matrix
class Matrix {
private:
    vector<vector<ComplexNumber *>> coords;
    int max = 4;
public:
    Matrix() = default;

    //Overloaded operator for addition of Matrices
    Matrix *operator+(const Matrix &matrix) const;

    //Overloaded operator for subtraction of Matrices
    Matrix *operator-(const Matrix &matrix) const;

    //Overloaded operator for multiplication of Matrices
    Matrix *operator*(const Matrix &matrix) const;

    //Overloaded operator for multiplication of Matrix by ComplexNumber
    Matrix *operator*(const ComplexNumber &complexNumber) const;

    //Prints matrix to file
    //@param res = ptr to file stream
    void printMatrixToFile(ofstream *res);


    //Adds line into matrix
    //@param lineToAdd = vector of ComplexNumbers
    void addLine(vector<ComplexNumber *> lineToAdd);
};

Matrix *Matrix::operator+(const Matrix &matrix) const {
    Matrix *newMatrix = new Matrix();
    for (unsigned long i = 0; i < max; ++i) {
        vector<ComplexNumber *> lineToAdd;
        for (unsigned long j = 0; j < max; ++j) {
            lineToAdd.emplace_back(*this->coords.at(i).at(j) + *matrix.coords.at(i).at(j));
        }
        newMatrix->addLine(lineToAdd);
    }
    return newMatrix;
}

Matrix *Matrix::operator-(const Matrix &matrix) const {
    Matrix *newMatrix = new Matrix();
    for (unsigned long i = 0; i < max; ++i) {
        vector<ComplexNumber *> lineToAdd;
        for (unsigned long j = 0; j < max; ++j) {
            lineToAdd.emplace_back(*this->coords.at(i).at(j) - *matrix.coords.at(i).at(j));
        }
        newMatrix->addLine(lineToAdd);
    }
    return newMatrix;
}

void Matrix::addLine(vector<ComplexNumber *> lineToAdd) {
    coords.emplace_back(lineToAdd);
}

void Matrix::printMatrixToFile(ofstream *res) {
    for (unsigned long i = 0; i < this->max; ++i) {
        for (unsigned long j = 0; j < max; ++j) {
            *res << coords.at(i).at(j)->toString() << "; ";
        }
        *res << endl;
    }
}

Matrix *Matrix::operator*(const Matrix &matrix) const {
    Matrix *newMatrix = new Matrix();
    for (unsigned long i = 0; i < max; ++i) {
        vector<ComplexNumber *> lineToAdd;
        for (unsigned long j = 0; j < max; ++j) {
            ComplexNumber *res = new ComplexNumber(0, 0);
            for (unsigned long k = 0; k < max; ++k) {
                res = *res + *(*this->coords.at(i).at(k) * *matrix.coords.at(k).at(j));
            }
            lineToAdd.emplace_back(res);
        }
        newMatrix->addLine(lineToAdd);
    }
    return newMatrix;
}

Matrix *Matrix::operator*(const ComplexNumber &complexNumber) const {
    Matrix *newMatrix = new Matrix();
    for (unsigned long i = 0; i < max; ++i) {
        vector<ComplexNumber *> lineToAdd;
        for (unsigned long j = 0; j < max; ++j) {
            lineToAdd.emplace_back(*this->coords.at(i).at(j) * complexNumber);
        }
        newMatrix->addLine(lineToAdd);
    }
    return newMatrix;
}

int main() {
    double re, im;
    string sLine;
    string srcFilepath, resFilepath;
    Matrix *matrix1 = new Matrix();
    Matrix *matrix2 = new Matrix();
    Matrix *matrix3 = new Matrix();
    cout << "Zadejte cestu k vstupnimu souboru:" << endl;
    cin >> srcFilepath;
    cout << "Zadejte cestu k vystupnimu souboru:" << endl;
    cin >> resFilepath;
    ifstream file;
//    srcFilepath = "/home/matty/zadani";
//    resFilepath = "/home/matty/reseni";
    file.open(srcFilepath);



    if (!file) {
        cerr << "Unable to open file " << srcFilepath << endl;
        return 1;
    }

    //Loading of matrices
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 4; ++j) {

            string src;
            getline(file, src);
            while (src[0] == '#' || src.empty()) {
                getline(file, src);
            }
            vector<ComplexNumber *> line;
            size_t index = 0;
            for (int k = 0; k < 4; ++k) {
                re = stod(src, &index);
                src = src.substr(index, src.length() - index);
                im = stod(src, &index);
                ++index;
                if (k < 3) {
                    ++index;
                    src = src.substr(index, src.length() - index);
                }
                ComplexNumber *complexNumber = new ComplexNumber(re, im);
                line.emplace_back(complexNumber);
            }
            if (i == 0) {
                matrix1->addLine(line);
            } else if (i == 1) {
                matrix2->addLine(line);
            } else {
                matrix3->addLine(line);
            }
        }
    }
    file.close();

    //Operation with matrices, done with overloaded operators
    ofstream  res;
    res.open(resFilepath);

    Matrix *matrixAdded = *(*matrix1 + *matrix2) + *matrix3;
    matrixAdded->printMatrixToFile(&res);

    res << endl;

    Matrix *matrixSubtracted = *(*matrix1 - *matrix2) - *matrix3;
    matrixSubtracted->printMatrixToFile(&res);

    res << endl;

    Matrix *matrix1MultipliedByComplexNumber = *matrix1 * *(new ComplexNumber(2.5, 3.3));
    matrix1MultipliedByComplexNumber->printMatrixToFile(&res);

    res << endl;

    Matrix *matrixMultiplied = *(*matrix1 * *matrix2) * *matrix3;
    matrixMultiplied->printMatrixToFile(&res);

    res.close();

    return 0;
}
